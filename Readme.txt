1. Ensure you have pip and python available

2. Go into the directory containing auto_django_setup_script.sh 

3. Run the script (sh auto_django_setup_script.sh on the terminal)	

4. Copy the folder 'housing_site' downloaded from bitbucket to the new folder created by the script named 'housing'

5. open the settings.py file to be found at :
		'\housing\housing_site\housing_site\settings.py'
	change the 'CURRENT_DIR' to your path to locate the db.sqlite3 file
	Provide an absolute path. The file is within '\housing\housing_site' folder.

6. open cmd and traverse to housing

7. run :
		.\scripts\activate
		cd housing_site
		python manage.py syncdb
		python manage.py runserver

8.	Good to go 
	Open url : 127.0.0.1:8000/account/login/
	for admin use: 127.0.0.1:8000/admin
	
	admin credentials:
					username :archit
					password :django29/05

For using the project :
1. The views for login-logout and registration are under the main site folder i.e.
    housing/housing-site/housing-site/views.py

2. templates for these pages are under directory
    housing/housing_site/templates

3. All other views can be found in userprofile folder i.e. 
     housing/housing_site/userprofile/views.py

4. Templates for these pages can be found in directory 
    housing/housing_site/userprofile/templates

5.  to see any user profile, you can use the url :
    127.0.0.1:8000/account/user/<username>/
