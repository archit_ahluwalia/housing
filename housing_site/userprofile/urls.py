from django.conf.urls import patterns, include , url
urlpatterns = patterns ('',
				url(r'^profile/$','userprofile.views.user_profile1'),
				url(r'^home/$', 'userprofile.views.home'),
				url(r'^user/(?P<user_id>.*)/$', 'userprofile.views.user'),
				url(r'^follow/(?P<user_id>.*)/$', 'userprofile.views.follow'),
				url(r'^unfollow/(?P<user_id>.*)/$', 'userprofile.views.unfollow'),
				url(r'^post/$', 'userprofile.views.tweet'),
)