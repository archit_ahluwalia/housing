from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class UserProfile(models.Model):														# model for userprofile
	user=models.OneToOneField(User )
	age=models.IntegerField(null=True)
	sex=models.CharField(max_length=10)
User.profile=property(lambda u :UserProfile.objects.get_or_create(user=u)[0])			# so that if a profile is not available it should create one 

class Post(models.Model):																# model for tweet
	body=models.TextField(null=True)
	date=models.DateTimeField('date of tweet')
	user=models.ForeignKey(User, default=True)


class Follow(models.Model):																# model containing a 'user' and 'follow' field
	user=models.ForeignKey(User ,related_name="user")									# each object implies a relation that the 
	follow=models.ForeignKey(User ,related_name="following")							# 'user' follows 'follow'