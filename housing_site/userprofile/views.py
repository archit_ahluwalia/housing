from django.shortcuts import render
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect
from django.contrib import auth
from django.core.context_processors import csrf
from userprofile.forms import UserProfileForm,PostForm
from django.contrib.auth.decorators import login_required
from userprofile.models import Post,Follow,UserProfile
from django.db.models import Q
from django.contrib.auth.models import User
import datetime

@login_required																					

def tweet(request):																				#view to post a tweet from a user account
	tweet=request.GET.get('tweet')
	author=request.user
	Post.objects.create(body=tweet,user=author,date=datetime.datetime.now())
	return HttpResponseRedirect('/account/home/')

def follow(request,user_id):																	# view for starting to follow some other user 	
	user1=User.objects.get(username=user_id) 													#'user1' here is the user to be followed
	p=Follow.objects.create(user=request.user, follow=user1)			
	return render_to_response('following.html', {'following' : user1})							# 'user' is the logged in user

def unfollow(request,user_id):																	# view to stop following a user 
	user1=User.objects.get(username=user_id) 													# 'user1' is the user to be stoped following
	p=Follow.objects.filter(user=request.user, follow=user1).delete()
	return render_to_response('unfollow.html' , {'unfollow' : user1})

def user(request,user_id=1):																	# handles requests to lookup a particular users profile
	f1=f2=0
	user=User.objects.get(username=user_id) 													#user profile to be looked
	profile=user.profile
	tweet=Post.objects.filter(user=user)
	follower=Follow.objects.filter(user=user , follow=request.user) 							# whether this user follows you or not
	if follower :
		f1=1

	following=Follow.objects.filter(user=request.user , follow=user) 							# whether you follow him or not
	if following: 
		f2=1
	return render_to_response('profile_other.html',{'profile':profile , 'tweets':tweet , 'f1' : f1, 'f2' :f2})


def user_profile1(request):																		# handles request to see our own profile (logged in user's profile)
	profile=UserProfile.objects.get(user=request.user)
	tweet=Post.objects.filter(user=request.user).order_by('-date')
	return render_to_response('profile.html',{'profile':profile , 'tweets':tweet})

def home(request):
	result = Follow.objects.filter(user=request.user)											# to get the people you follow
	result2 = Follow.objects.filter(follow=request.user)										# to get the people who follow you
	post = Post.objects.filter(Q(user=request.user )											# to get your latest tweets and those of others whom you follow 
								|Q(user=result)).order_by('-date')
	return render_to_response('home.html',
								{'followings': result ,'posts': post ,'followers' : result2})