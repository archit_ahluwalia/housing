from userprofile.models import UserProfile,Post,Follow
from django.contrib import admin

admin.site.register(Post)
admin.site.register(UserProfile)
admin.site.register(Follow)
# 
# Register your models here.

