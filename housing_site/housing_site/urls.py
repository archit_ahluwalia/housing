from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',

    url(r'^account/', include('userprofile.urls')),                     #route to userprofile application
    url(r'^admin/', include(admin.site.urls)),
    url(r'^account/login/$', 'housing_site.views.login'),               
    url(r'^account/auth/$', 'housing_site.views.auth_view'),
    url(r'^account/logout/$', 'housing_site.views.logout'),
    url(r'^account/loggedin/$', 'housing_site.views.loggedin'),
    url(r'^account/invalid/$', 'housing_site.views.invalid_login'),
    url(r'^account/register/$', 'housing_site.views.register_user'),
	url(r'^account/register_success/$', 'housing_site.views.register_success'),

)
