from django.shortcuts import render
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect
from django.contrib import auth
from django.core.context_processors import csrf
from django.contrib.auth.forms import UserCreationForm
from forms import RegisterForm
from django.contrib.auth.decorators import login_required

#default django authorisation used 

def login(request):													#view to handle login requests
	c={}
	c.update(csrf(request))
	return render_to_response('login.html',c)

def auth_view(request):
	username=request.POST.get('username','')
	password=request.POST.get('password','')
	user=auth.authenticate(username=username,password=password)

	if user is not None:
		auth.login(request,user)
		return HttpResponseRedirect('/account/home')
	else: 
		return HttpResponseRedirect('/account/invalid')

def loggedin(request):
	return render_to_response('home.html' ,{'full_name':request.user.username})

def invalid_login(request):
	return render_to_response('invalid_login.html')

def logout(request):												#view to handle logout request	
	auth.logout(request)
	return render_to_response('logout.html')

def register_user(request):											#view to handle register request
	if request.method=='POST':
		form=RegisterForm(request.POST)
		if form.is_valid():
			form.save()
			return HttpResponseRedirect('/account/register_success')
	args={}
	args.update(csrf(request))

	args['form']=RegisterForm()
	return render_to_response('register.html',args)

def register_success(request):
	return render_to_response('register_success.html')
