from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm

class RegisterForm(UserCreationForm):											# form to add additional fields in registeration form
	email=forms.EmailField(required=True)										# fields added: email, age ,sex 
	age=forms.IntegerField()				
	sex=forms.CharField(max_length=10)

	class Meta:
		model=User
		fields =('username', 'email' , 'password1' , 'password2','age' ,'sex')
	def save(self,commit=True):
		user= super(RegisterForm, self).save(commit=False)
		user.email=self.cleaned_data['email']
		user.age=self.cleaned_data['age']
		user.sex=self.cleaned_data['sex']

		if commit:
			user.save()
		return user