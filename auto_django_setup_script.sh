#!/bin/bash

# clear terminal window
# ensure you have pip
clear

echo
echo
echo "Lets start.."
echo
pip install virtualenv

echo
echo
echo "Creating new virtual environment --> housing"
echo
echo
virtualenv housing --no-site-packages

echo
echo
echo "Activating our virtual environment --> housing "
echo
echo
. housing/scripts/activate

echo
echo
echo "Installing Django"          
echo
echo
pip install django

# echo
# echo "Setting up the database for django.."
# echo
# cd housing_site
# python manage.py syncdb
# python manage.py runserver